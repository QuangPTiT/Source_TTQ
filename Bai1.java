/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

import java.util.*;

/**
 *
 * @author Windows 10 Pro
 */
public class Bai1 {
    private static Scanner scan = new Scanner(System.in);
    private static ArrayList<Book> Books;
    private static ArrayList<Person> Persons;
    
    static  void themBanDoc()
    {
        System.out.print("Nhap So Luong Ban Doc Can Them: ");
        int n=scan.nextInt();
        for(int i=0;i<n;i++)
        {
            System.out.println("Nhan Ban Doc Can Them Thu "+(i+1));
            Person person= new Person();
            person.input();
            Persons.add(person);
        }
    }
    
    static  void xuatBanDoc()
    {
        for(Person person : Persons)
            person.output();
    }
    
    static  void themSach()
    {
        System.out.print("Nhap So Luong Sach Can Them: ");
        int n=scan.nextInt();
        for(int i=0;i<n;i++)
        {
            System.out.println("Nhan Sach Can Them Thu "+(i+1));
            Book book= new Book();
            book.input();
            Books.add(book);
        }
    }
    
    static void xuatSach()
    {
        for(Book book : Books)
            book.output();
    }
    
    static  void doMuonSach()
    {
        for(Person person : Persons)
        {
            System.out.println("Nhap So Luong Cuon Sach Can Muon Cho Ban Doc Co MA "+person.getCodePerson());
            int n=scan.nextInt();
            for(int i=0;i<n;i++)
            {
                System.out.println("Nhap MA Cuon Sach Muon: ");
                int ID=scan.nextInt();
                for(Book book : Books)
                {
                    if(ID==book.getCodeBook())
                    {
                        if(superContanins(book,person.Books) || book.getNumber()==0)
                        {
                            System.out.println("Da Muon Cuon Sach Nay Roi Hoac Sach Da Het !!!");
                            i--;
                        }
                        else
                        {
                            Book bookTemp= new Book(book.getCodeBook(),book.getNameBook(),book.getActorBook(),book.getSpecialized(),book.getYear(),1);
                            person.Books.add(bookTemp); 
                            book.setNumber(book.getNumber()-1);
                        }
                        break;
                    }   
                }
            }
        }
    }
    
    static boolean superContanins(Book b,ArrayList<Book> listBook)
    {
        for(Book book : listBook)
        {
            if(book.getCodeBook()==b.getCodeBook())
                return true;
        }
        return false;
    }
    
    static void xuatDanhSach()
    {
        for(Person person:Persons)
        {
            person.output();
            for(Book book : person.Books)
            {
                book.output();
            }
        }
    }
    
    static  void seach()
    {
        int ID= scan.nextInt();
        for(Person person : Persons)
        {
            if(person.getCodePerson()==ID)
            {
                person.output();
                for(Book book : person.Books)
                {
                   book.output(); 
                }
                return;
            }
        }
        System.out.println("Khong co ban doc nhu vay !!!");
    }
    
    static void menu()
    {
        Books= new ArrayList<>();
        Persons= new ArrayList<>();
        boolean stop= true;
        while(stop)
        {
            System.out.println("=====MENU======");
            System.out.println("1.Them Ban Doc");
            System.out.println("2.Them Sach");
            System.out.println("3.Danh Sach Ban Doc");
            System.out.println("4.Danh Sach Sach");
            System.out.println("5.Muon Sach");
            System.out.println("6.Danh Sach Muon");
            System.out.println("7.Sap Xep Danh Sach Muon Theo Ho Va Ten");
            System.out.println("8.Sap Xep Danh Sach Muon Theo So Luong Cuon Sach Giam Dan");
            System.out.println("9.Tim Kiem");
            System.out.println("0.Thoat");
            System.out.println("===============");
            int choise ;
            choise=scan.nextInt();
            switch(choise)
            {
                case 1:
                {
                    System.out.println("=====THEM BAN DOC=====");
                    themBanDoc();
                    break;
                }
                case 2:
                {
                    System.out.println("=====THEM SACH====");
                    themSach();
                    break;
                }
                case 3:
                {
                    System.out.println("======DANH SACH BAN DOC======");
                    xuatBanDoc();
                    break;
                }
                case 4:
                {
                    System.out.println("======DANH SACH SACH=======");
                    xuatSach();
                    break;
                }
                case 5:
                {
                    System.out.println("======MUON SACH======");
                    doMuonSach();
                    break;
                }
                case 6:
                {
                    System.out.println("======DANH SACH MUON=====");
                    xuatDanhSach();
                    break;
                }
                case 7:
                {
                    System.out.println("======SAP XEP THEO TEN=====");
                    break;
                }
                case 8:
                {
                    System.out.println("=====SAP XEP THEO SO LUONG SACH GIAM DAN======");
                    break;
                }
                case 9:
                {
                    System.out.println("=====TIM KIEM=====");
                    seach();
                    break;
                }
                default :
                {
                    System.out.println("BYE BYE !!!");
                    stop=false;
                    break;
                }
            }
        }
    }
    
    
    public static void main(String[] args) {
        menu();
    }
}

class Book
{
    private Scanner scanner= new Scanner(System.in);
    public Book()
    {
        //input();
    }
    
    public Book(int code,String nameBook, String actorBook,typeSpecialized specialized,int year,int number )
    {
       // super();
        this.codeBook=code;
        this.nameBook=nameBook;
        this.actorBook=actorBook;
        this.specialized=specialized;
        this.year=year;
        this.number=number;
    }
    private int codeBook;
    private void setCode(int codeBook)
    {
        this.codeBook=codeBook;
    }
    public int getCodeBook()
    {
        return this.codeBook;
    }
    private String nameBook;
    public String getNameBook()
    {
        return nameBook;
    }
    private String actorBook;
    public String getActorBook()
    {
        return actorBook;
    }
    private typeSpecialized specialized;
    public typeSpecialized getSpecialized()
            {
                return specialized;
            }
    private int year;
    public int getYear()
    {
        return year;
    }
    private int number;
    public int getNumber()
    {
        return number;
    }
    public void setNumber(int number)
    {
        this.number=number;
    }
    
    private Person person;
    public void input()
    {
        setCode(countBook.temp++);
        System.out.print("Nhap Ten Sach: ");
        this.nameBook=scanner.nextLine();
        System.out.print("Nhap Ten Tac Gia: ");
        this.actorBook=scanner.nextLine();
        System.out.println("Nhap Chuyen Nganh: ");
        int temp;
        do
        {
            temp=scanner.nextInt();
            if(temp==0)
                specialized=typeSpecialized.Khoa_Hoc_Tu_Nhien;
            if(temp==1)
                specialized=typeSpecialized.Van_Hoc_Nghe_Thuat;
            if(temp==2)
                specialized=typeSpecialized.Dien_Tu_Vien_Thong;
            if(temp==3)
                specialized=typeSpecialized.Cong_Nghe_Thong_Tin;
        }while(temp<0 || temp >3);
        System.out.print("Nhap Nam: ");
        year= scanner.nextInt();
        System.out.print("Nhap So Luong: ");
        number=scanner.nextInt();
    }
    
    public void output()
    {
        System.out.println("\tMa Sach: "+this.codeBook);
        System.out.println("Ten Sach: "+this.nameBook);
        System.out.println("Tac Gia: "+this.actorBook);
        System.out.println("Chuyen Nganh: "+this.specialized);
        System.out.println("Nam Xuat Ban: "+this.year);
        System.out.println("So Luong: "+this.number);
    }
}

enum typeSpecialized
{
    Khoa_Hoc_Tu_Nhien,
    Van_Hoc_Nghe_Thuat,
    Dien_Tu_Vien_Thong,
    Cong_Nghe_Thong_Tin,
}

class countBook
{
    public static  int temp=10000;
}

class countPerson
{
    public static  int temp=10000;
}
class Person
{
    private Scanner scanner= new Scanner(System.in);
    public Person()
    {
        //input();
    }
    
    private int codePerson;
    private void setCode(int code)
    {
        this.codePerson=code;
    }
    
    public int getCodePerson()
    {
        return this.codePerson;
    }
    private String namePerson;
    private String address;
    private String phoneNumber;
    private typeSex sex;
    public ArrayList<Book> Books;
    
    
    public void output()
    {
        System.out.println("\tMa Ban Doc: "+this.codePerson);
        System.out.println("Ho Va Ten: "+this.namePerson);
        System.out.println("Gioi Tinh: "+this.sex);
        System.out.println("Dia Chi: "+this.address);
        System.out.println("SDT : "+this.phoneNumber);
    }
    
    public void input()
    {
        Books= new ArrayList<>();
        setCode(countPerson.temp++);
        System.out.print("Nhap Ho Va Ten: ");
        this.namePerson=scanner.nextLine();
        System.out.print("Nhap Dia Chi: ");
        address= scanner.nextLine();
        System.out.print("Nhap SDT: ");
        phoneNumber=scanner.nextLine();
        System.out.print("Nhap Gioi Tinh: ");
        int temp;
        do
        {
            temp=scanner.nextInt();
            if(temp==0)
                sex=typeSex.Male;
            if(temp==1)
                sex=typeSex.Fmale;
            if(temp==2)
                sex=typeSex.Other;
        }while(temp<0 || temp >2);
        
    }
}

enum typeSex
{
    Male,
    Fmale,
    Other
}


